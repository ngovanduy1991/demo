package defs;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.actors.Stage;
import net.thucydides.core.environment.SystemEnvironmentVariables;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.webdriver.DevToolsWebDriverFacade;
import net.thucydides.core.webdriver.SupportedWebDriver;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import net.thucydides.core.webdriver.WebDriverFactory;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import task.Navigate;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class MyStepdefs {


    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("uesr");
    }

    @Given("Tom open page")
    public void tomOpenPage() throws InterruptedException {
        Actor actor = theActorInTheSpotlight();

        actor.attemptsTo(
                Navigate.toTheHomePage()
        );
//
//        Serenity.getWebdriverManager().getRegisteredDrivers().forEach(d -> System.out.println(d.getClass()));
//        String url = "http://gooogle.com";
//
//        actor.can(BrowseTheWeb.with(
//                new DevToolsWebDriverFacade(new FirefoxDriver(), new WebDriverFactory())));
//
//        actor.attemptsTo(
//
//                Open.url(url)
//        );
//
//        System.out.println("DRiver " + Serenity.getWebdriverManager().getRegisteredDrivers().size());
//
//        //Thread.sleep(5000);
//
//        Serenity.getWebdriverManager().getRegisteredDrivers().forEach(d -> System.out.println(d.getClass()));
//        BrowseTheWeb.as(actor).withDriver(Serenity.getWebdriverManager().getRegisteredDrivers().get(0));
//        actor.attemptsTo(
//                Open.url(url)
//        );
//
//        Thread.sleep(5000);
    }
}
